#include "valeur.hpp"

Valeur::Valeur(double value, std::string const &etudiant) : value(value), etudiant(etudiant) {}

double Valeur::getNombre() const {
	return value;
}

void Valeur::setNombre(double _value) {
	value = _value;
}

double Valeur::getNote() const {
	return getNombre();
}

void Valeur::setNote(double _note) {
	setNombre(_note);
}

std::string const &Valeur::getEtudiant() const {
	return etudiant;
}

void Valeur::setEtudiant(std::string const &_etudiant) {
	etudiant = std::string(_etudiant);
}
