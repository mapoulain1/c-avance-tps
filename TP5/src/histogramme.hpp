#pragma once

#include "valeur.hpp"

#include "classe.hpp"
#include "echantillon.hpp"
#include <functional>
#include <map>
#include <set>
#include <ostream>

template <typename Comp = std::less<Classe>>
class Histogramme {
	std::set<Classe, Comp> classes;
	std::multimap<Classe, Valeur> valeurs;

  public:
	Histogramme(double min, double max, int nbClasses);

	template <typename OtherComp>
	Histogramme(Histogramme<OtherComp> const &other);

	std::set<Classe, Comp> const &getClasses() const;
	void ajouter(Echantillon const &echantillon);

	std::multimap<Classe, Valeur> const &getValeurs() const;
	auto getValeurs(Classe const &classe) const;
	
};
bool operator<(std::pair<const Classe, Valeur> const &pair, Classe const &classe);
bool operator<(Classe const &classe, std::pair<const Classe, Valeur> const &pair);

template <typename Comp>
std::ostream& operator<<(std::ostream& os, Histogramme<Comp> const & histo);

#include "histogramme.hxx"