#pragma once

#include "valeur.hpp"
#include <vector>

class Echantillon {
	std::vector<Valeur> valeurs;

  public:
	Echantillon();
	Echantillon(double value);
	unsigned int getTaille() const;
	void ajouter(Valeur const &valeur);
	Valeur getMinimum() const;
	Valeur getMaximum() const;
	Valeur &getValeur(unsigned long index);
	Valeur const &getValeur(unsigned long index) const;
};