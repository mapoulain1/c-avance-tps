#include "classe.hpp"

Classe::Classe(double min, double max) : min(min), max(max), quantity(0) {
}

double Classe::getBorneInf() const {
	return min;
}

double Classe::getBorneSup() const {
	return max;
}

double Classe::getQuantite() const {
	return quantity;
}

void Classe::setBorneInf(double inf) {
	min = inf;
}

void Classe::setBorneSup(double sup) {
	max = sup;
}

void Classe::setQuantite(double qte) {
	quantity = qte;
}

void Classe::ajouter() {
	quantity++;
}

bool Classe::operator<(const Classe &rhs) const {
	return min < rhs.getBorneInf();
}

bool Classe::operator>(const Classe &rhs) const {
	return rhs < *this;
}
