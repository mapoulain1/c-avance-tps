#pragma once

#include <string>

class Valeur {
	double value;
	std::string etudiant;

  public:
	Valeur(double value = 0, std::string const &etudiant = "inconnu");
	double getNombre() const;
	void setNombre(double _value);
	double getNote() const;
	void setNote(double _note);
	std::string const &getEtudiant() const;
	void setEtudiant(std::string const &_etudiant);
};