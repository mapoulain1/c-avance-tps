#pragma once

class Classe {
	double min;
	double max;
    unsigned quantity;

  public:
	Classe(double min, double max);

	double getBorneInf() const;
	double getBorneSup() const;
	double getQuantite() const;

	void setBorneInf(double inf);
	void setBorneSup(double sup);
	void setQuantite(double qte);

    void ajouter();
	bool operator<(const Classe& rhs) const;
	bool operator>(const Classe& rhs) const;
};

