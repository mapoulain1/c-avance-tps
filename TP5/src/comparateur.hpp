#pragma once

#include "classe.hpp"

template <typename T>
class ComparateurQuantite {
  public:
	bool operator()(T const &lhs, T const &rhs) const {
		return lhs < rhs;
	}
};

template <>
class ComparateurQuantite<Classe> {
  public:
	bool operator()(Classe const &lhs, Classe const &rhs) const {
		if (lhs.getQuantite() != rhs.getQuantite())
			return lhs.getQuantite() > rhs.getQuantite();
		return lhs < rhs;
	}
};