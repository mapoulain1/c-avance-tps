#include <algorithm>
#include <iostream>

using std::endl;

template <typename Comp>
Histogramme<Comp>::Histogramme(double min, double max, int nbClasses) : classes(), valeurs() {
	double delta = (max - min) / nbClasses;
	for (int i = 0; i < nbClasses; i++)
		classes.emplace(Classe{min + i * delta, min + (i + 1) * delta});
}

template <typename Comp>
template <typename OtherComp>
Histogramme<Comp>::Histogramme(Histogramme<OtherComp> const &other) {
	for (auto &c : other.getClasses())
		classes.insert(Classe(c));
}

template <typename Comp>
std::set<Classe, Comp> const &Histogramme<Comp>::getClasses() const {
	return classes;
}

template <typename Comp>
void Histogramme<Comp>::ajouter(Echantillon const &echantillon) {
	unsigned long size = echantillon.getTaille();
	for (unsigned long i = 0; i < size; i++) {
		double current = echantillon.getValeur(i).getNombre();
		auto found = std::find_if(classes.begin(), classes.end(), [current](Classe const &x) {
			return x.getBorneInf() <= current && x.getBorneSup() > current;
		});
		if (found != classes.end()) {
			Classe c{found->getBorneInf(), found->getBorneSup()};
			c.setQuantite(found->getQuantite() + 1);
			classes.erase(found);
			classes.insert(c);
			valeurs.insert(std::make_pair<Classe, Valeur>(std::move(c), current));
		}
	}
}

template <typename Comp>
std::multimap<Classe, Valeur> const &Histogramme<Comp>::getValeurs() const {
	return valeurs;
}
template <typename Comp>
auto Histogramme<Comp>::getValeurs(Classe const &classe) const {
	return std::equal_range(valeurs.begin(), valeurs.end(), classe);
}

template <typename Comp>
std::ostream &operator<<(std::ostream &os, Histogramme<Comp> const &histo) {
	for (auto &classe : histo.getClasses()) {
		auto valeurs = histo.getValeurs(classe);
		os << "[" << classe.getBorneInf() << ";" << classe.getBorneSup() << "] =  " << std::count_if(valeurs.first, valeurs.second, [&classe](auto &pair) { return pair.first.getBorneInf() == classe.getBorneInf(); }) << " : ";
		for (; valeurs.first != valeurs.second; valeurs.first++) {
			os << "(" << (valeurs.first)->second.getEtudiant() << ";" << (valeurs.first)->second.getNote() << ") ";
		}
		os << endl;
	}
	return os;
}

bool operator<(std::pair<const Classe, Valeur> const &pair, Classe const &classe) {
	return pair.first.getBorneInf() < classe.getBorneInf();
}

bool operator<(Classe const &classe, std::pair<const Classe, Valeur> const &pair) {
	return pair.first.getBorneInf() > classe.getBorneInf();
}