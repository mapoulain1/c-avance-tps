#include "echantillon.hpp"

#include <algorithm>
#include <stdexcept>

Echantillon::Echantillon() : valeurs() {}

Echantillon::Echantillon(double value) : Echantillon() {
	valeurs.push_back(value);
}

unsigned int Echantillon::getTaille() const {
	return valeurs.size();
}

void Echantillon::ajouter(Valeur const &valeur) {
	valeurs.push_back(valeur);
}

Valeur Echantillon::getMinimum() const {
	if (valeurs.size() == 0)
		throw std::domain_error("Echantillon est vide");

	auto found = std::min_element(valeurs.begin(), valeurs.end(), [](auto const &a, auto const &b) {
		return a.getNombre() < b.getNombre();
	});
	return *found;
}

Valeur Echantillon::getMaximum() const {
	if (valeurs.size() == 0)
		throw std::domain_error("Echantillon est vide");

	auto found = std::min_element(valeurs.begin(), valeurs.end(), [](auto const &a, auto const &b) {
		return a.getNombre() > b.getNombre();
	});
	return *found;
}

Valeur &Echantillon::getValeur(unsigned long index) {
	if (index < 0 || index >= valeurs.size()) {
		throw std::out_of_range(std::to_string(index));
	}
	return valeurs[index];
}

Valeur const &Echantillon::getValeur(unsigned long index) const {
	if (index < 0 || index >= valeurs.size()) {
		throw std::out_of_range(std::to_string(index));
	}
	return valeurs[index];
}