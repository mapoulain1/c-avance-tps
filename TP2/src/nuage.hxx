#include "polaire.hpp"
#include <iostream>
#include <vector>

template <class T, typename C>
typename Nuage<T, C>::const_iterator Nuage<T, C>::begin() const {
	return points.begin();
}

template <class T, typename C>
typename Nuage<T, C>::const_iterator Nuage<T, C>::end() const {
	return points.end();
}

template <class T, typename C>
std::size_t Nuage<T, C>::size() const {
	return points.size();
}

template <class T, typename C>
void Nuage<T, C>::ajouter(T const &point) {
	points.push_back(point);
}

template <class T, typename C>
T barycentre_v1(Nuage<T, C> const &nuage) {
	return barycentre_v2(nuage);
}

template <class U>
typename U::value_type barycentre_v2(U const &nuage) {
	using T = typename U::value_type;
	uint total = nuage.size();
	if (total == 0)
		return T{0, 0};

	double x = 0;
	double y = 0;
	for (auto point : nuage) {
		Cartesien tmp = {};
		point.convertir(tmp);
		x += tmp.getX();
		y += tmp.getY();
	}

	Cartesien barycentre{x / total, y / total};
	T barycentre_generic;
	barycentre.convertir(barycentre_generic);
	return barycentre_generic;
}

template <>
Polaire barycentre_v1(Nuage<Polaire> const &nuage) {
	uint total = nuage.size();
	if (total == 0)
		return {0, 0};

	double angle = 0;
	double distance = 0;
	for (Polaire point : nuage) {
		angle += point.getAngle();
		distance += point.getDistance();
	}
	return {angle / total, distance / total};
}