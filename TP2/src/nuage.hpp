#pragma once

#include "point.hpp"
#include <vector>

template <class T, typename C = std::vector<T>>
class Nuage {
	using vector_point = C;
	vector_point points;

  public:
	using const_iterator = typename vector_point::const_iterator;
	using value_type = T;
	const_iterator begin() const;
	const_iterator end() const;
	std::size_t size() const;
	void ajouter(T const &point);
};

template <class T, typename C>
T barycentre_v1(Nuage<T, C> const &nuage);

template <class U>
typename U::value_type barycentre_v2(U const &nuage);

template <>
Polaire barycentre_v1(Nuage<Polaire> const &nuage);

#include "nuage.hxx"