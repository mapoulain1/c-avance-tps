// Entetes //---------------------------------------------------------------------------------------
#include "catch.hpp"

#include <sstream>
#include <typeinfo>

#include "cartesien.hpp"
#include "nuage.hpp"
#include "point.hpp"
#include "polaire.hpp"
#include "vecteur.hpp"

// Tests //-----------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------ 1
TEST_CASE("TP1_Polaire::Constructeur") {
	const double a = 12.0;
	const double d = 24.0;

	Polaire p(a, d);

	REQUIRE(p.getAngle() == Approx(a));
	REQUIRE(p.getDistance() == Approx(d));
}

//------------------------------------------------------------------------------------------------ 2
TEST_CASE("TP1_Polaire::ConstructeurDefaut") {
	Polaire p;

	REQUIRE(p.getAngle() == Approx(0.0));
	REQUIRE(p.getDistance() == Approx(0.0));
}

//------------------------------------------------------------------------------------------------ 3
TEST_CASE("TP1_Polaire::Accesseurs") {
	const double a = 12.0;
	const double d = 24.0;

	Polaire p(13.0, 25.0);

	p.setAngle(a);
	p.setDistance(d);

	REQUIRE(p.getAngle() == Approx(a));
	REQUIRE(p.getDistance() == Approx(d));
}

//------------------------------------------------------------------------------------------------ 4
TEST_CASE("TP1_Polaire::AccesseursConstants") {
	const Polaire p;

	REQUIRE(p.getAngle() == Approx(0.0));
	REQUIRE(p.getDistance() == Approx(0.0));
}

//------------------------------------------------------------------------------------------------ 5
TEST_CASE("TP1_Polaire::Affichage") {
	Polaire p(12.0, 24.0);
	std::stringstream flux;

	p.afficher(flux);

	REQUIRE(flux.str() == "(a=12;d=24)");
}

//------------------------------------------------------------------------------------------------ 6
TEST_CASE("TP1_Cartesien::Constructeur") {
	const double x = 12.0;
	const double y = 24.0;

	Cartesien c(x, y);

	REQUIRE(c.getX() == Approx(x));
	REQUIRE(c.getY() == Approx(y));
}

//------------------------------------------------------------------------------------------------ 7
TEST_CASE("TP1_Cartesien::ConstructeurDefaut") {
	Cartesien c;

	REQUIRE(c.getX() == Approx(0.0));
	REQUIRE(c.getY() == Approx(0.0));
}

//------------------------------------------------------------------------------------------------ 8
TEST_CASE("TP1_Cartesien::Accesseurs") {
	const double x = 12.0;
	const double y = 24.0;

	Cartesien c(13.0, 25.0);

	c.setX(x);
	c.setY(y);

	REQUIRE(c.getX() == Approx(x));
	REQUIRE(c.getY() == Approx(y));
}

//------------------------------------------------------------------------------------------------ 9
TEST_CASE("TP1_Cartesien::AccesseursConstants") {
	const Cartesien c;

	REQUIRE(c.getX() == Approx(0.0));
	REQUIRE(c.getY() == Approx(0.0));
}

//----------------------------------------------------------------------------------------------- 10
TEST_CASE("TP1_Cartesien::Affichage") {
	Cartesien c(12.0, 24.0);
	std::stringstream flux;

	c.afficher(flux);

	REQUIRE(flux.str() == "(x=12;y=24)");
}

//----------------------------------------------------------------------------------------------- 11
TEST_CASE("TP1_Point::AffichageVirtuel") {
	Polaire p(12.0, 24.0);
	Cartesien c(13.0, 25.0);

	const Point &p1 = p;
	const Point &p2 = c;

	std::stringstream flux1;
	std::stringstream flux2;

	p1.afficher(flux1);
	p2.afficher(flux2);

	REQUIRE(flux1.str() == "(a=12;d=24)");
	REQUIRE(flux2.str() == "(x=13;y=25)");
}

//----------------------------------------------------------------------------------------------- 12
TEST_CASE("TP1_Point::OperateurFlux") {
	Polaire p(12.0, 24.0);
	Cartesien c(13.0, 25.0);

	const Point &p1 = p;
	const Point &p2 = c;

	std::stringstream flux1;
	std::stringstream flux2;

	flux1 << p1;
	flux2 << p2;

	REQUIRE(flux1.str() == "(a=12;d=24)");
	REQUIRE(flux2.str() == "(x=13;y=25)");
}

//----------------------------------------------------------------------------------------------- 13
TEST_CASE("TP1_Point::ConversionVersPolaire_V1") {
	const double x = 12.0;
	const double y = 24.0;
	const double a = 63.434948;
	const double d = 26.832815;

	const Cartesien c(x, y);
	Polaire p;

	c.convertir(p);

	REQUIRE(p.getDistance() == Approx(d).epsilon(1e-3));
	REQUIRE(p.getAngle() == Approx(a).epsilon(1e-3));
}

//----------------------------------------------------------------------------------------------- 14
TEST_CASE("TP1_Point::ConversionVersCartesien_V1") {
	const double a = 12.0;
	const double d = 24.0;
	const double x = 23.475542;
	const double y = 4.9898805;

	const Polaire p(a, d);
	Cartesien c;

	p.convertir(c);

	REQUIRE(c.getX() == Approx(x).epsilon(1e-3));
	REQUIRE(c.getY() == Approx(y).epsilon(1e-3));
}

//----------------------------------------------------------------------------------------------- 15
TEST_CASE("TP1_Point::ConversionVirtuel") {
	const double x = 12.0;
	const double y = 24.0;
	const double a = 63.434948;
	const double d = 26.832815;

	Cartesien c(x, y);
	Polaire p(a, d);

	const Point *x1 = &c;
	const Point *x2 = &p;

	Cartesien c1;
	Cartesien c2;
	Polaire p1;
	Polaire p2;

	x1->convertir(c1);
	x1->convertir(p1);
	x2->convertir(c2);
	x2->convertir(p2);

	REQUIRE(c1.getX() == Approx(x).epsilon(1e-3));
	REQUIRE(c1.getY() == Approx(y).epsilon(1e-3));
	REQUIRE(c2.getX() == Approx(x).epsilon(1e-3));
	REQUIRE(c2.getY() == Approx(y).epsilon(1e-3));

	REQUIRE(p1.getAngle() == Approx(a).epsilon(1e-3));
	REQUIRE(p1.getDistance() == Approx(d).epsilon(1e-3));
	REQUIRE(p2.getAngle() == Approx(a).epsilon(1e-3));
	REQUIRE(p2.getDistance() == Approx(d).epsilon(1e-3));
}

//----------------------------------------------------------------------------------------------- 16
TEST_CASE("TP1_Point::ConversionVersPolaire_V2") {
	const double x = 12.0;
	const double y = 24.0;
	const double a = 63.434948;
	const double d = 26.832815;

	Cartesien c(x, y);
	Polaire p(c);

	REQUIRE(p.getAngle() == Approx(a).epsilon(1e-3));
	REQUIRE(p.getDistance() == Approx(d).epsilon(1e-3));
}

//----------------------------------------------------------------------------------------------- 17
TEST_CASE("TP1_Point::ConversionVersCartesien_V2") {
	const double a = 12.0;
	const double d = 24.0;
	const double x = 23.475542;
	const double y = 4.9898805;

	Polaire p(a, d);
	Cartesien c(p);

	REQUIRE(c.getX() == Approx(x).epsilon(1e-3));
	REQUIRE(c.getY() == Approx(y).epsilon(1e-3));
}

//----------------------------------------------------------------------------------------------- 18
TEST_CASE("TP1_Nuage::Ajout") {
	Cartesien p1(12.0, 24.0);
	Polaire p2(13.0, 25.0);
	Polaire p3(p1);
	Cartesien p4(p2);

	Nuage n;

	REQUIRE(n.size() == 0u);

	n.ajouter(p1);
	n.ajouter(p2);
	n.ajouter(p3);
	n.ajouter(p4);

	REQUIRE(n.size() == 4u);
}

//----------------------------------------------------------------------------------------------- 19
TEST_CASE("TP1_Nuage::Iterateurs") {
	Cartesien p1(12.0, 24.0);
	Polaire p2(13.0, 25.0);
	Polaire p3(p1);
	Cartesien p4(p2);

	Nuage n;

	n.ajouter(p1);
	n.ajouter(p2);
	n.ajouter(p3);
	n.ajouter(p4);

	Point *t[4];
	unsigned i = 0;
	Nuage::const_iterator it = n.begin();

	while (it != n.end())
		t[i++] = *(it++);

	REQUIRE(typeid(*(t[0])) == typeid(Cartesien));
	REQUIRE(typeid(*(t[1])) == typeid(Polaire));
	REQUIRE(typeid(*(t[2])) == typeid(Polaire));
	REQUIRE(typeid(*(t[3])) == typeid(Cartesien));

	Cartesien &p5 = *static_cast<Cartesien *>(t[0]);
	Polaire &p6 = *static_cast<Polaire *>(t[1]);
	Polaire &p7 = *static_cast<Polaire *>(t[2]);
	Cartesien &p8 = *static_cast<Cartesien *>(t[3]);

	REQUIRE(p5.getX() == Approx(12.0));
	REQUIRE(p5.getY() == Approx(24.0));
	REQUIRE(p6.getAngle() == Approx(13.0));
	REQUIRE(p6.getDistance() == Approx(25.0));

	REQUIRE(p7.getAngle() == Approx(63.434948).epsilon(1e-3));
	REQUIRE(p7.getDistance() == Approx(26.832815).epsilon(1e-3));
	REQUIRE(p8.getX() == Approx(24.359251).epsilon(1e-3));
	REQUIRE(p8.getY() == Approx(5.623776).epsilon(1e-3));
}

//--------------------------------------------------------------------------------------Commun 20-22
double x[] = {3, 7, 13, 27};
double y[] = {4, 8, 16, 32};

Cartesien p1(x[0], y[0]);
Cartesien p2(x[1], y[1]);
Cartesien p3(x[2], y[2]);
Cartesien p4(x[3], y[3]);

Nuage n;

//----------------------------------------------------------------------------------------------- 20
TEST_CASE("TP1_Nuage::Barycentre") {
	n.ajouter(p1);
	n.ajouter(p2);
	n.ajouter(p3);
	n.ajouter(p4);

	Cartesien b = barycentre(n);

	REQUIRE(b.getX() == Approx((x[0] + x[1] + x[2] + x[3]) / 4));
	REQUIRE(b.getY() == Approx((y[0] + y[1] + y[2] + y[3]) / 4));
}

//----------------------------------------------------------------------------------------------- 21
TEST_CASE("TP1_Nuage::BarycentreCartesien") {
	Cartesien b = BarycentreCartesien()(n);

	REQUIRE(b.getX() == Approx((x[0] + x[1] + x[2] + x[3]) / 4));
	REQUIRE(b.getY() == Approx((y[0] + y[1] + y[2] + y[3]) / 4));
}

//----------------------------------------------------------------------------------------------- 22
TEST_CASE("TP1_Nuage::BarycentrePolaire") {
	Polaire p(Cartesien((x[0] + x[1] + x[2] + x[3]) / 4, (y[0] + y[1] + y[2] + y[3]) / 4));
	Polaire b = BarycentrePolaire()(n);

	REQUIRE(b.getAngle() == Approx(p.getAngle()));
	REQUIRE(b.getDistance() == Approx(p.getDistance()));
}

//----------------------------------------------------------------------------------------------- 23
TEST_CASE("TP1_Vecteur::Constructor") {
	Vecteur v;
	REQUIRE(v.capacity() == 10);
	REQUIRE(v.size() == 0);
}

//----------------------------------------------------------------------------------------------- 24
TEST_CASE("TP1_Vecteur::Push_back") {
	Vecteur v;
	v.push_back(5);
	v.push_back(4);
	v.push_back(6);
	v.push_back(8);
	v.push_back(9);
	REQUIRE(v.size() == 5);
}

//----------------------------------------------------------------------------------------------- 25
TEST_CASE("TP1_Vecteur::Push_back_alloc") {
	Vecteur v;
	for (size_t i = 0; i < 15; i++)
		v.push_back(i);

	REQUIRE(v.size() == 15);
	REQUIRE(v.capacity() == 20);
}

//----------------------------------------------------------------------------------------------- 26
TEST_CASE("TP1_Vecteur::Push_back_alloc_advenced") {
	Vecteur v;
	Vecteur::setCapacityIncrease(100);

	for (size_t i = 0; i < 103; i++)
		v.push_back(i);

	Vecteur::setCapacityIncrease(10);

	REQUIRE(v.size() == 103);
	REQUIRE(v.capacity() == 110);
}

//----------------------------------------------------------------------------------------------- 26
TEST_CASE("TP1_Vecteur::Pop_back") {
	Vecteur v;
	for (size_t i = 0; i < 15; i++)
		v.push_back(i);

	REQUIRE(v.pop_back() == 14);
	REQUIRE(v.pop_back() == 13);
}

//----------------------------------------------------------------------------------------------- 26
TEST_CASE("TP1_Vecteur::Flux") {
	Vecteur v;
	std::stringstream ss;

	for (size_t i = 0; i < 3; i++)
		v.push_back(i);
	ss << v;

	REQUIRE(ss.str() == "[0,1,2]");
}

//----------------------------------------------------------------------------------------------- 27
TEST_CASE("TP1_Vecteur::Concat+=") {
	Vecteur v1;
	Vecteur v2;

	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	v2.push_back(10);
	v2.push_back(20);
	v2.push_back(30);

	v1 += v2;

	std::stringstream ss;

	ss << v1;

	REQUIRE(ss.str() == "[1,2,3,10,20,30]");
}

//----------------------------------------------------------------------------------------------- 27
TEST_CASE("TP1_Vecteur::Concat+=2") {
	Vecteur v1;
	Vecteur v2;

	for (int i = 0; i < 15; i++) {
		v1.push_back(i);
		v2.push_back(20 + i);
	}

	v1 += v2;
	std::stringstream ss;
	ss << v1;

	REQUIRE(ss.str() == "[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34]");
}

//----------------------------------------------------------------------------------------------- 27
TEST_CASE("TP1_Vecteur::Concat+") {
	Vecteur v1;
	Vecteur v2;

	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	v2.push_back(10);
	v2.push_back(20);
	v2.push_back(30);

	Vecteur &v3 = v1 + v2;

	std::stringstream ss;

	ss << v3;

	REQUIRE(ss.str() == "[1,2,3,10,20,30]");
}

// Fin //-------------------------------------------------------------------------------------------
