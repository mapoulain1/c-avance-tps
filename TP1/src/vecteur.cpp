#include "vecteur.hpp"
#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>

using std::ostream;

size_t Vecteur::capacityIncrease = 10;

Vecteur::Vecteur(size_t const capacity) : m_size(0), m_capacity(capacity), m_data(nullptr) {
	m_data = new int[m_capacity];
}

Vecteur::Vecteur(Vecteur const &other) : Vecteur(other.m_capacity) {
	std::memcpy(m_data, other.m_data, m_capacity);
	m_size = other.m_size;
}

Vecteur::~Vecteur() {
	delete[] m_data;
}

Vecteur &Vecteur::operator=(Vecteur const &other) {
	m_size = other.m_size;
	m_capacity = other.m_capacity;
	delete[] m_data;
	m_data = new int[m_capacity];
	std::memcpy(m_data, other.m_data, m_capacity);
	return *this;
}

Vecteur &Vecteur::operator+=(Vecteur const &other) {
	if (other.size() + m_size > m_capacity) {
		int *tmp = new int[other.m_size + m_size];
		std::memcpy(tmp, m_data, sizeof(int) * m_size);
		std::memcpy(tmp + m_size, other.m_data, sizeof(int) * other.m_size);
		delete[] m_data;
		m_data = tmp;
		m_size += other.m_size;
		m_capacity = m_size;
	} else {
		std::cout << "coucou" << std::endl;
		std::memcpy(m_data + m_size, other.m_data, other.m_size * sizeof(int));
		m_size += other.m_size;
	}
	return *this;
}

size_t Vecteur::size() const {
	return m_size;
}

int Vecteur::capacity() const {
	return m_capacity;
}

int Vecteur::get(size_t const index) const {
	if (index >= m_capacity)
		throw std::invalid_argument{"Index can't be greater than capacity"};
	return m_data[index];
}

void Vecteur::set(size_t const index, int const value) {
	if (index >= m_capacity)
		throw std::invalid_argument{"Index can't be greater than capacity"};
	m_data[index] = value;
}

void Vecteur::push_back(int const value) {
	if (m_size >= m_capacity) {
		int *tmp = new int[m_capacity + capacityIncrease];
		std::memcpy(tmp, m_data, m_capacity * sizeof(int));
		delete[] m_data;
		m_data = tmp;
		m_capacity += capacityIncrease;
	}
	m_data[m_size] = value;
	m_size++;
}

int Vecteur::pop_back() {
	if (m_size == 0) {
		throw std::invalid_argument{"Vector is already empty"};
	}
	m_size--;
	return m_data[m_size];
}

void Vecteur::setCapacityIncrease(size_t const value) {
	if (value < 1) {
		throw std::invalid_argument{"Increase value must be greater than 1"};
	}
	capacityIncrease = value;
}

ostream &operator<<(ostream &ss, const Vecteur &vector) {
	ss << '[';
	if (vector.size() == 0) {
		ss << ']';
		return ss;
	}

	for (size_t i = 0; i < vector.size() - 1; i++) {
		ss << (int)vector.get(i) << ',';
	}
	ss << (int)vector.get(vector.size() - 1) << ']';
	return ss;
}

Vecteur &operator+(Vecteur &lhs, Vecteur const &rhs) {
	lhs += rhs;
	return lhs;
}