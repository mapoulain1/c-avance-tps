#include "vecteur.hpp"
#include <cstdlib>
#include <iostream>

using std::cout;
using std::endl;

int main() {
	Vecteur v;

	for (int i = 0; i < 15; i++) {
		v.push_back(i);
		cout << v << endl;
	}
	cout << v << endl;

	return EXIT_SUCCESS;
}
