#pragma once

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

class Vecteur {
	size_t m_size;
	size_t m_capacity;
	int *m_data;
	static size_t capacityIncrease;

  public:
	Vecteur(size_t const capacity = 10);
	Vecteur(Vecteur const &other);
	~Vecteur();
	Vecteur &operator=(Vecteur const &other);
	Vecteur &operator+=(Vecteur const &other);
	friend Vecteur &operator+(Vecteur &lhs, Vecteur const &rhs);
	size_t size() const;
	int capacity() const;
	int get(size_t const index) const;
	void set(size_t const index, int const value);
	void push_back(int const value);
	int pop_back();
	static void setCapacityIncrease(size_t const value);
};

std::ostream &operator<<(std::ostream &ss, Vecteur const &vector);
Vecteur &operator+(Vecteur &lhs, Vecteur const &rhs);