#pragma once
#include "demangle.hpp"
#include "exception.hpp"
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>

template <class T>
std::string chaine(T &input);

template <>
std::string chaine<std::string>(std::string &input);

template <>
std::string chaine<double>(double &input);

template <>
std::string chaine<int>(int &input);

template <class... T>
std::string chaine(T &&...inputs);

template <class... T>
std::string chaine(std::tuple<T...> &tuple);

template <typename T, size_t... Is>
std::string chaineHelperTuple(T &t, std::index_sequence<Is...> is);

template <class T>
std::string chaine(T &input) {
	throw ExceptionChaine(demangle(typeid(input).name()));
}

template <>
std::string chaine<std::string>(std::string &input) {
	return input;
}

template <>
std::string chaine<double>(double &input) {
	return std::to_string(input);
}

template <>
std::string chaine<int>(int &input) {
	return std::to_string(input);
}

template <class... T>
std::string chaine(T &&...inputs) {
	std::stringstream ss;
	ss << std::fixed << std::setprecision(6);

	// Compressed
	((ss << chaine(inputs) << " "), ...);

	// Or Lambda
	// auto const foreach = [&ss](auto elem) { ss << chaine(elem) << " "; };
	//(foreach (inputs), ...);

	return ss.str();
}

template <class... T>
std::string chaine(std::tuple<T...> &tuple) {
	return chaineHelperTuple(tuple, std::make_index_sequence<sizeof...(T)>());
}

template <typename T, size_t... Is>
std::string chaineHelperTuple(T &t, std::index_sequence<Is...> is) {
	return chaine(std::get<Is>(t)...);
}