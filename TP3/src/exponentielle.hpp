#pragma once
#include "puissance.hpp"
#include "factorielle.hpp"

template <int N>
class Exponentielle {
  public:
	inline static double valeur(double const &value) {
		return Exponentielle<N - 1>::valeur(value) 
        + Puissance<N>::valeur(value) 
        / Factorielle<N>::valeur;
	}
};

template <>
class Exponentielle<0> {
  public:
	inline static double valeur(double const &value) {
		return 1;
	}
};