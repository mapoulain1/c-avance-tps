#pragma once

template <int N>
class Puissance {
  public:
	inline static double valeur(double const &value){
		return Puissance<N - 1>::valeur(value) * value;
	}
};

template <>
class Puissance<0> {
  public:
	inline static double valeur(double const &value) {
		return 1;
	}
};