#pragma once
#include "puissance.hpp"
#include "factorielle.hpp"

template <int N>
class Sinus {
  public:
	inline static double valeur(double const &value){
		return (Puissance<N>::valeur(-1)
      	 * Puissance<(2 * N) + 1>::valeur(value)
         / Factorielle<(2 * N) + 1>::valeur) 
		 + Sinus<N - 1>::valeur(value);
	}
};

template <>
class Sinus<0> {
  public:
	inline static double valeur(double const &value) {
		return value;
	}
};