#pragma once
#include "puissance.hpp"
#include "factorielle.hpp"

template <int N>
class Cosinus {
  public:
	inline static double valeur(double const &value){
		return Cosinus<N - 1>::valeur(value)
         + (Puissance<N>::valeur(-1)
         * Puissance<2 * N>::valeur(value)
         / Factorielle<2 * N>::valeur);
	}
};

template <>
class Cosinus<0> {
  public:
	inline static double valeur(double const &value) {
		return 1;
	}
};