#pragma once
#include <exception>
#include <string>

class ExceptionChaine : public std::exception {
	std::string type;
	std::string error;

  public:
	ExceptionChaine(std::string type);
	virtual const char *what() const noexcept override;
};

ExceptionChaine::ExceptionChaine(std::string type) : type(type),
													 error("Conversion en chaine impossible pour '" + type + "'") {}

const char *ExceptionChaine::what() const noexcept {
	return error.c_str();
}