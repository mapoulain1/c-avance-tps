#pragma once

#include "carte.hpp"
#include <memory>

class UsineCarte {
	int numberOfCards;
	int maxNumberOfCards;
	UsineCarte(UsineCarte const &) = delete;
	UsineCarte &operator=(UsineCarte const &);

  public:
	std::unique_ptr<Carte> getCarte();
	UsineCarte(int maxCards = 52);
};