#pragma once

#include <memory>
#include <ostream>
#include <vector>

class Ressource {
	int stock;

  public:
	Ressource(int stock);
	void consommer(int n);
	int getStock() const;
};

using ressources_t = std::vector<std::weak_ptr<Ressource>>;
std::ostream &operator<<(std::ostream &ss, ressources_t const &ressources);