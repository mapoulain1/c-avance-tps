#include "carte.hpp"

unsigned int Carte::counter;

Carte::Carte(int value) : value(value) {
	counter++;
}

unsigned int Carte::getValeur() const {
	return value;
}

Carte &Carte::operator=(Carte const &) {
	return *this;
}

unsigned int Carte::getCompteur() {
	return counter;
}

Carte::~Carte() {
	counter--;
}