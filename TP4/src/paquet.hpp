#pragma once

#include "carte.hpp"
#include "usine.hpp"
#include <iostream>
#include <memory>
#include <vector>

using paquet_t = std::vector<std::unique_ptr<Carte>>;

void remplir(paquet_t &paquet, UsineCarte &usine);
std::ostream &operator<<(std::ostream &ss, paquet_t const &paquet_t);