#include "ressource.hpp"
#include <string>

Ressource::Ressource(int stock) : stock(stock) {}

void Ressource::consommer(int n) {
	stock -= n;
	if (stock < 0)
		stock = 0;
}

int Ressource::getStock() const {
	return stock;
}

std::ostream &operator<<(std::ostream &ss, ressources_t const &ressources) {
	for (auto &ressource : ressources) {
		if (auto ptr = ressource.lock())
			ss << ptr->getStock() << " ";
		else
			ss << "- ";
	}
	return ss;
}