#pragma once

class UsineCarte;

class Carte {
	static unsigned int counter;
	unsigned int value;
	Carte(int value);
	Carte &operator=(Carte const &);
	Carte(Carte const &) = delete;

  public:
	unsigned int getValeur() const;
	static unsigned int getCompteur();
	~Carte();
	friend class UsineCarte;
};