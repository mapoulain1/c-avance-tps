#include "usine.hpp"

std::unique_ptr<Carte> UsineCarte::getCarte() {
	if (numberOfCards >= maxNumberOfCards)
		return nullptr;
	return std::unique_ptr<Carte>(new Carte(numberOfCards++));
}

UsineCarte &UsineCarte::operator=(UsineCarte const &) {
	return *this;
}

UsineCarte::UsineCarte(int maxCard) : numberOfCards(0), maxNumberOfCards(maxCard) {}