#pragma once

#include "ressource.hpp"
#include <memory>

class Consommateur {
	int besoin;
	std::shared_ptr<Ressource> ressource;

  public:
	Consommateur(int besoin, std::shared_ptr<Ressource> ressource);
	void puiser();
};