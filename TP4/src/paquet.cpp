#include "paquet.hpp"

void remplir(paquet_t &paquet, UsineCarte &usine) {
	while (auto card = usine.getCarte())
		paquet.push_back(std::move(card));
}

std::ostream &operator<<(std::ostream &ss, paquet_t const &paquet) {
	for (auto &card : paquet)
		ss << card.get()->getValeur() << " ";
	return ss;
}