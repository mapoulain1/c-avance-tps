#include "consommateur.hpp"

Consommateur::Consommateur(int besoin, std::shared_ptr<Ressource> ressource) : besoin(besoin), ressource(ressource) {}

void Consommateur::puiser() {
	if (ressource == nullptr)
		return;

	ressource.get()->consommer(besoin);
	if (ressource.get()->getStock() == 0) {
		ressource.reset();
		ressource = nullptr;
	}
}